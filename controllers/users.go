package controllers

import (
  "fmt"
  "gitlab.com/jabreland/lenslocked/v2/views"
  "net/http"
)

// NewsUsers is used to create a new Users controllers.
// This function should only be used during initial setup
// as it will panic if the template is not correct
func NewUsers() *Users {
  return &Users{
    NewView: views.NewView("bootstrap", "views/users/new.gohtml"),
  }
}

type Users struct {
  NewView *views.View
}

type SignupForm struct {
  Email    string `schema:"email"`
  Password string `schema:"password"`
}

// New is used to render the form where a user can
// create a new user account
//
// GET /signup
func (u *Users) New(w http.ResponseWriter, r *http.Request) {
  if err := u.NewView.Render(w, nil); err != nil {
    panic(err)
  }
}

// Create is used to process the sign up form when a user
// submits it. This is used to create a new user account
//
// POST /signup
func (u *Users) Create(w http.ResponseWriter, r *http.Request) {
  var form SignupForm

  if err := parseForm(r, &form); err != nil {
    panic(err)
  }

  fmt.Fprintln(w, form)
}
