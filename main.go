package main

import (
  "github.com/gorilla/mux"
  "gitlab.com/jabreland/lenslocked/v2/controllers"
  "net/http"
)



func main() {
  staticController := controllers.NewStatic()
  usersController := controllers.NewUsers()

  router := mux.NewRouter()
  router.Handle("/", staticController.Home).Methods("GET")
  router.Handle("/contact", staticController.Contact).Methods("GET")
  router.HandleFunc("/signup", usersController.New).Methods("GET")
  router.HandleFunc("/signup", usersController.Create).Methods("POST")
  http.ListenAndServe(":3000", router)
}

func must(err error) {
  if err != nil {
    panic(err)
  }
}
