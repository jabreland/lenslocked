module gitlab.com/jabreland/lenslocked/v2

go 1.14

require (
	github.com/gorilla/mux v1.7.4
	github.com/gorilla/schema v1.1.0
)
